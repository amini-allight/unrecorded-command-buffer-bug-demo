# Unrecorded Command Buffer Bug Demo

A demonstration of a bug in RADV where submission of an unrecorded command buffer causes the entire GPU driver to lock up. Submitting an unrecorded command buffer is an invalid operation under the Vulkan specification but locking up in response to it makes development work very difficult. This has existed from at least 21.2.4 (and in reality much earlier) up to and including the current version at the time of writing, 23.1.6.

**WARNING:** Obviously if you run this with RADV **your computer will crash**. It seems to be safe on AMDVLK: on version 2023.Q3.1 the call to `vkQueueSubmit` returns `VK_ERROR_UNKNOWN` and the program exits harmlessly. Other platforms and runtimes have not been tested.
